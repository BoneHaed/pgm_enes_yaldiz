from pgmpy.models import BayesianModel
from pgmpy.factors.discrete import TabularCPD


blood_model = BayesianModel([('M_C_P_1', 'M_C_C'),
                             ('P_C_P_1', 'M_C_C'),
                             ('M_C_P_2', 'P_C_C'),
                             ('P_C_P_2', 'P_C_C'),
                             ('M_C_C', 'Blood'),
                             ('P_C_C', 'Blood'),
                             ('Blood', 'Blut_Test')])

M_C_P_1_cpd = TabularCPD(
    variable='M_C_P_1',
    variable_card=2,
    values=[[.1, .9]]
)

P_C_P_1_cpd = TabularCPD(
    variable='P_C_P_1',
    variable_card=2,
    values=[[.2, .8]]
)

M_C_P_2_cpd = TabularCPD(
    variable='M_C_P_2',
    variable_card=2,
    values=[[.15, .85]]
)

P_C_P_2_cpd = TabularCPD(
    variable='P_C_P_2',
    variable_card=2,
    values=[[.18, .82]]
)

M_C_C_cpd = TabularCPD(
    variable='M_C_C',
    variable_card=2,
    values=[[.9, .4, .3, 0],
            [.1, .6, .7, 1]],
    evidence=['M_C_P_1', 'P_C_P_1'],
    evidence_card=[2, 2]
)

P_C_C_cpd = TabularCPD(
    variable='P_C_C',
    variable_card=2,
    values=[[.95, .45, .35, 0],
            [.05, .55, .65, 1]],
    evidence=['M_C_P_2', 'P_C_P_2'],
    evidence_card=[2, 2]
)

Blood_cpd = TabularCPD(
    variable='Blood',
    variable_card=2,
    values=[[.91, .45, .35, 0],
            [.09, .55, .65, 1]],
    evidence=['M_C_C', 'P_C_C'],
    evidence_card=[2, 2]
)

Blut_Test_cpd = TabularCPD(
    variable='Blut_Test',
    variable_card=2,
    values=[[.99, 0],
            [0.01, 1]],
    evidence=['Blood'],
    evidence_card=[2]
)
# Add the relationships to your models

blood_model.add_cpds(M_C_P_1_cpd, P_C_P_1_cpd, M_C_P_2_cpd,
                     P_C_P_2_cpd, M_C_C_cpd, P_C_C_cpd, Blood_cpd, Blut_Test_cpd)

print(blood_model.check_model())

print(blood_model.is_active_trail('M_C_P_1', 'Blut_Test'))

print(blood_model.is_active_trail(
    'M_C_P_1', 'Blut_Test', observed=['M_C_C']))

print(blood_model.local_independencies('M_C_C'))

# print(blood_model.get_independencies())
